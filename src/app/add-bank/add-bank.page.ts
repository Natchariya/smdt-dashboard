import { Component, OnInit, NgZone } from '@angular/core';
import { MenuController, NavController, AlertController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { setError } from "../services/customvalidator.validator";
import { BankService } from '../services/bank.service';

@Component({
  selector: 'app-add-bank',
  templateUrl: './add-bank.page.html',
  styleUrls: ['./add-bank.page.scss'],
})
export class AddBankPage implements OnInit {
  previewBase64;
  addBankForm: FormGroup;
  isSubmitted: boolean = false;
  loading = false;
  constructor(public menuCtrl: MenuController,public bank:BankService,public zone:NgZone,public navCtrl:NavController,public alertControler: AlertController) { 
    this.addBankForm = new FormGroup({
      bank_name: new FormControl("", Validators.required),
      account_number: new FormControl("", [Validators.required,Validators.pattern('[0-9]*')]),
      account_type: new FormControl("", Validators.required),
      account_holder_firstname: new FormControl("", [Validators.required,Validators.pattern('[ก-๏]*')]),
      account_holder_lastname: new FormControl("", [Validators.required,Validators.pattern('[ก-๏]*')]),
      account_branch: new FormControl("", [Validators.required]),
      image_book_and_id: new FormControl(null, Validators.required),
      
    },{
      validators: setError
    });
  }

  ngOnInit() {
    
  }
  submitForm() {
    this.zone.run(()=>{
      this.isSubmitted = true;
      if (!this.addBankForm.valid) {
        return false;
      } else {
        this.loading = true;
        this.bank.request_bank(this.addBankForm.value).then(()=>{
          this.navCtrl.navigateRoot("/bank-account")
          this.loading = false;
        }).catch(err=>{
          this.loading = false;
        })
      }
    })
  }
  async presentAlert() {
    const alert = await this.alertControler.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }
  get getFormControl() {
    return this.addBankForm.controls;
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
  file(e,control) {
    this.zone.run(()=>{
      this.readURL(e.target).then(e=>{
        this.previewBase64 = e;
      })
      this.addBankForm.patchValue({[control]: e.target.files[0]})
    })

  }
  readURL(input) {
    return new Promise((reslove,reject)=>{
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e:any)=> {
          reslove(e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }else{
        reslove(undefined);
      }
    })

  }

}
