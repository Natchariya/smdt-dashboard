import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AngularFireAuthGuard, hasCustomClaim, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';
import { map } from 'rxjs/operators';
import { AppComponent } from './app.component';
// const adminOnly = () => hasCustomClaim('admin');
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['usr/login']);
const redirectauthorizedToDashboard = () => redirectLoggedInTo(['dashboard']);

// const redirectLoggedInToItems = () => redirectLoggedInTo(['items']);
// const belongsToAccount = (next) => hasCustomClaim(`account-${next.params.id}`)

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardPageModule),
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin }
  },
  {
    path: 'usr',
    loadChildren: () => import('./no-auth/no-auth.module').then(m => m.NoAuthPageModule),
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectauthorizedToDashboard }
  },
  {
    path: 'kyc',
    loadChildren: () => import('./kyc/kyc.module').then(m => m.KycPageModule),
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin }
  },
  {
    path: 'bank-account',
    loadChildren: () => import('./bank-account/bank-account.module').then(m => m.BankAccountPageModule),
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin }
  },
  {
    path: 'add-bank',
    loadChildren: () => import('./add-bank/add-bank.module').then(m => m.AddBankPageModule),
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin }
  },
  {
    path: 'training',
    loadChildren: () => import('./training/training.module').then(m => m.TrainingPageModule),
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin }
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
