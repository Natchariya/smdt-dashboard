import { Component, OnInit } from '@angular/core';

import { Platform, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { environment } from "../environments/environment";
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex;
  public appPages = [
    {
      title: 'Dashboard',
      url: '/dashboard',
      icon: 'speedometer',
    },
    {
      title: 'ตั้งค่าบัญชีผู้ใช้',
      url: '/folder/Inbox',
      icon: 'assets/svg/userset.svg',
      sub_menu: [
        {
          title: 'ยืนยันตัวตน',
          url: '/kyc',
          icon: 'assets/svg/userset.svg',
        },
        {
          title: 'บัญชีธนาคาร',
          url: '/bank-account',
          icon: 'assets/svg/userset.svg',
        }
      ]
    },
    {
      title: 'Training Course',
      url: '/training',
      icon: 'easel'
    },
    {
      title: 'siammodintech channel',
      url: 'https://www.youtube.com/channel/UCK3jUYvxlR1M6g9ugw5mWAA',
      icon: 'logo-youtube'
    },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  public server = environment.server_version;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private auth: AuthService,
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public user: UserService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.menuCtrl.enable(false);
      // this.statusBar.styleDefault();
      // this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.router.events.subscribe(res=>{
      const path = this.router.url.split("/")[1];
      this.selectedIndex = `/${path}`;
      this.appPages.forEach(element => {
        if(element.sub_menu){
          element.sub_menu.forEach(sub => {
            if(this.selectedIndex == sub.url){
              element['isOpen'] = true;
            }
          });
        }
      });
    })
  }
  openSubmenu(index){
    if(this.appPages[index].sub_menu){
      this.appPages[index]["isOpen"] = !this.appPages[index]["isOpen"];
    }else{
      if(this.appPages[index].url.slice(0,4) == "http"){
        window.open(this.appPages[index].url,'_blank')
      }else{
        this.navCtrl.navigateRoot(this.appPages[index].url)
      }
    }
    
  }
  LinkSubmenu(path){
    this.navCtrl.navigateRoot(path)
  }
  logout(){
    this.auth.logout().then(()=>{
      this.navCtrl.navigateRoot("usr")
      // window.
    })
  }
  openMenu(){
    this.menuCtrl.toggle();
  }
}
