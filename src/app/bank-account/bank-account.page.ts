import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { BankService } from '../services/bank.service';

@Component({
  selector: 'app-bank-account',
  templateUrl: './bank-account.page.html',
  styleUrls: ['./bank-account.page.scss'],
})
export class BankAccountPage implements OnInit {
  bank_list;
  constructor(public menuCtrl: MenuController,public bank: BankService) { 
    

  }

  ngOnInit() {
    this.bank.get_bank();
    this.bank_list = this.bank.data;
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
  
}
