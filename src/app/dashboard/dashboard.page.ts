import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { DashboardService } from '../services/dashboard.service';
import { KycService } from '../services/kyc.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  myOpts = {
    decimalPlaces: 3
  }
  MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  @ViewChild('imageCanvas', { static: false }) canvas: any;
  chartData: ChartDataSets[] = [{
    data: [],
    label: 'ยอดเงินที่ได้',
    lineTension: 0,       
  }];
  chartLabels: Label[];
  // Options
  chartOptions = {
    responsive: true,
    title: {
      display: false,
      text: 'Income and Target'
    },
  };
  chartColors: Color[] = [
    {
      borderColor: '#3EC7F5',
      backgroundColor: '#00000000'
    },
    {
      borderColor: '#EF5C53',
      backgroundColor: '#00000000'
    }
  ];
  chartType = 'line';
  showLegend = false;
  canvasElement: any;
  display = false;
  alert = true;
  dash_data:any;
  commit_data;
  sum_income;
  graph;
  kyc;
  constructor(public menuCtrl: MenuController, public dashboard: DashboardService,public kycs: KycService,public navCtrl:NavController) {
    this.dashboard.getCommition();
    this.dashboard.getContacts();
    this.dash_data = this.dashboard.data
    this.commit_data = this.dashboard.commit_data
    this.sum_income = this.dashboard.sum_income;
    this.graph = this.dashboard.graph_data.subscribe(res => {
      this.chartData[0].data = res.data;
      this.chartLabels = res.days;
    })
    this.kycs.getState();
    this.kyc = this.kycs.state;

    // this.chartData[0].data = this.graph.data;
    // this.chartLabels = this.graph.days
  }
  Movekyc(){
    this.navCtrl.navigateRoot("/kyc")
  }
  getDays() {
    let i = new Date(new Date().getFullYear(), 5, 0).getDate();
    let days = [];
    for (let index = 1; index < i + 1; index++) {
      days.push(index);
    }
    return days;
  }
  ngOnInit() {
    setTimeout(() => {
      this.display = true;
    }, 500);
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
  ngAfterViewInit() {
  }
  isApprove() {
    
  }
}
