import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { KycService } from 'src/app/services/kyc.service';
import { NavController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class StepGuard implements CanActivate {
  private uid = firebase.auth().currentUser.uid;
  constructor(private kyc: KycService, public navCtrl: NavController, private router: Router, private fdb: AngularFirestore) {

  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // console.log("gae");
    // const segments = route.url;
    // this.kyc.getState();
    // let statesss = this.kyc.state.subscribe(on=>{
    //   return on.pipe(map(m=>{
    //     console.log(m);
        
        return true;
    //   }))
    // })
    // return statesss;
    // if (this.kyc.isActivate) {
    //   return true;
    // }
    
    // switch (segments[0].path) {
    //   case "step1":
    //     return this.router.parseUrl("kyc/step3");
    //     break;
    //   case "step2":
    //     return this.router.parseUrl("kyc/step3");
    //     break;
    //   case "step3":
    //     return true;
    //     break;
    //   case "step4":
    //     return this.router.parseUrl("kyc/step3");
    //     break;
    //   default:
    //     return this.router.parseUrl("kyc/step3");
    //     break;
    // }

    // return this.kyc.state.pipe(map((d:any)=>{
    //   console.log(d);
    //   return true;
    // }));
    // return this.fdb.collection("users").doc(this.uid).valueChanges().pipe(map((res:any)=>{
    //   if(res.kyc_state){
    //     return true;
    //   }else{
    //     return false;
    //   }
    // }));
    // return this.kyc.state.pipe(map(states=>{
    //   // const segments = route.url;
    //   console.log(states);
    //   if(states){
    //     console.log(states);
    //     return true
    //     // if(states == "reviewing" || states == "reject"){
    //     //   switch (segments[0].path) {
    //     //     case "step1":
    //     //       return this.router.parseUrl("kyc/step3");
    //     //       break;
    //     //     case "step2":
    //     //       return this.router.parseUrl("kyc/step3");
    //     //       break;
    //     //     case "step3":
    //     //       return true;
    //     //       break;
    //     //     case "step4":
    //     //       return this.router.parseUrl("kyc/step3");
    //     //       break;
    //     //     default:
    //     //       return this.router.parseUrl("kyc/step3");
    //     //       break;
    //     //   }
    //     // }else{
    //     //   if(segments[0].path != "step4"){
    //     //     return this.router.parseUrl("kyc/step4");
    //     //   }
    //     //   return true;
    //     // }

    //   }else{
    //     return this.router.parseUrl("kyc/step1");
    //     // if(segments[0].path == "step1" || segments[0].path == "step2"){
    //     //   return true;
    //     // }

    //   }
    // }));
    // }

  }
}
