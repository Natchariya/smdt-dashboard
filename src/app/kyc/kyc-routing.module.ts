import { NgModule } from '@angular/core';
import { Routes, RouterModule,UrlTree } from '@angular/router';

import { KycPage } from './kyc.page';
import { StepGuard } from './guard/step.guard';
import { AngularFireAuthGuard, hasCustomClaim } from '@angular/fire/auth-guard';
import { map } from 'rxjs/operators';
const adminOnly = () => hasCustomClaim('admin');
const redirectToProfileEditOrLogin = () => map(user => user ? ['kyc', 'step1'] : ['dashboard']);
const routes: Routes = [
  {
    path: '',
    redirectTo: 'step1',
    pathMatch: 'full'
  },
  {
    path: '',
    component: KycPage,
    children: [
      {
        path: 'step1',
        loadChildren: () => import('./step1/step1.module').then( m => m.Step1PageModule),
        // canActivate: [AngularFireAuthGuard],
        // data: { authGuardPipe: redirectToProfileEditOrLogin }
        // canActivate: [StepGuard]
      },
      {
        path: 'step2',
        loadChildren: () => import('./step2/step2.module').then( m => m.Step2PageModule),
        // canActivate: [StepGuard]
      },
      {
        path: 'step3',
        loadChildren: () => import('./step3/step3.module').then( m => m.Step3PageModule),
        // canActivate: [StepGuard]
      },
      {
        path: 'step4',
        loadChildren: () => import('./step4/step4.module').then( m => m.Step4PageModule),
        // canActivate: [StepGuard]
      },
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KycPageRoutingModule {}
