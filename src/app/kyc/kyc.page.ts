import { Component, OnInit, HostListener } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { KycService } from '../services/kyc.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.page.html',
  styleUrls: ['./kyc.page.scss'],
})

export class KycPage implements OnInit {
  step;
  state;
  loading = true;
  constructor(public menuCtrl: MenuController, public router: Router, public navCtrl: NavController, public kyc: KycService) {
    
    
    // .subscribe(res=>{
    //   console.log(res);
      

      
    // })
  }

  ngOnInit() {
    this.kyc.getState();
    this.loading = true;
    this.state = this.kyc.state.subscribe(res=>{
      if(res.state == "reject"){
        this.navCtrl.navigateRoot("/kyc/step3")
      }else if(res.state == "reviewing"){
        this.navCtrl.navigateRoot("/kyc/step3")
      }else if(res.state == "approve"){
        this.navCtrl.navigateRoot("/kyc/step4")
      }else{
        this.navCtrl.navigateRoot("/kyc/step1")
      }
      setTimeout(() => {
        this.loading = false;
      }, 700);
    })


    this.router.events.subscribe(ob => {
      const path = this.router.url.split('/')[2];
      this.step = path;
    })

  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
  ngAfterViewInit() {

  }
  ngOnDestroy() {
    this.state.unsubscribe();
    this.kyc.distory();
    this.kyc.isStart = false;
  }

}
