import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, AlertController } from '@ionic/angular';
import { MoreThenTwentry } from "../../services/customvalidator.validator";
import { KycService } from 'src/app/services/kyc.service';
@Component({
  selector: 'app-step1',
  templateUrl: './step1.page.html',
  styleUrls: ['./step1.page.scss','../kyc.page.scss'],
})
export class Step1Page implements OnInit {
  step1Form: FormGroup;
  err_message: string;
  submitted = false;
  same_address = false;
  // minDate = new Date().getFullYear();
  constructor(public zone: NgZone,public navCtrl: NavController,public alertCtrl: AlertController,public kyc:KycService) {

   }

  ngOnInit() {
    this.kyc.isStart = false;
    this.step1Form = new FormGroup({
      firstname_eng: new FormControl("", Validators.required),
      lastname_eng: new FormControl("", Validators.required),
      firstname_th: new FormControl("", Validators.required),
      lastname_th: new FormControl("", Validators.required),
      gender: new FormControl("", Validators.required),
      marital_status: new FormControl("", Validators.required),
      tel_number: new FormControl("", Validators.required),
      dfb: new FormControl("", [Validators.required,MoreThenTwentry]),
      income_range: new FormControl(""),
      nationality: new FormControl(""),
      job: new FormControl(""),
      form_id_card_address: new FormControl("", Validators.required),
      form_id_card_subdistrict: new FormControl("", Validators.required),
      form_id_card_district: new FormControl("", Validators.required),
      form_id_card_province: new FormControl("", Validators.required),
      form_id_card_zipcode: new FormControl("", Validators.required),
      current_address: new FormControl("", Validators.required),
      current_subdistrict: new FormControl("", Validators.required),
      current_district: new FormControl("", Validators.required),
      current_province: new FormControl("", Validators.required),
      current_zipcode: new FormControl("", Validators.required),
    });
    if(this.kyc.getForm()){
      this.step1Form.patchValue(this.kyc.getForm());
    }
    
    this.step1Form.valueChanges.subscribe(res=>{
      this.kyc.isStart = true;
    })
  }
  get getFormControl() {
    return this.step1Form.controls;
  }
  sameChange(e){
    console.log(e);
    this.zone.run(()=>{
      this.same_address = e.detail.checked;
      this.sameChecked();
    })
  }
  sameChecked(){
    if(this.same_address){
      this.step1Form.patchValue({
        current_address: this.step1Form.value.form_id_card_address,
        current_subdistrict: this.step1Form.value.form_id_card_subdistrict,
        current_district: this.step1Form.value.form_id_card_district,
        current_province: this.step1Form.value.form_id_card_province,
        current_zipcode: this.step1Form.value.form_id_card_zipcode,
      })
    }
  }
  onSubmit(){
    this.zone.run(()=>{
      this.submitted = true;
      this.sameChecked();
      if (!this.step1Form.invalid) {
        this.navCtrl.navigateForward("/kyc/step2")
        this.kyc.SaveState(this.step1Form.value,1);
      }else{
        console.table(this.step1Form.value);
      }
    })
  }
}
