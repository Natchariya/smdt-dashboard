import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MoreThenTwentry } from 'src/app/services/customvalidator.validator';
import { KycService } from 'src/app/services/kyc.service';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.page.html',
  styleUrls: ['./step2.page.scss','../kyc.page.scss'],
})
export class Step2Page implements OnInit {
  step2Form: FormGroup;
  submitted = false;
  previewBase64 = {
    image_front_pid: "",
    image_back_pid: "",
    image_hold_pid: "",
  }
  loading = false;
  constructor(public zone: NgZone,public navCtrl: NavController,public alertCtrl: AlertController,public kyc:KycService) { 
    this.step2Form = new FormGroup({
      pid_front: new FormControl("", Validators.required),
      pid_back: new FormControl("", Validators.required),
      image_front_pid: new FormControl("", Validators.required),
      image_back_pid: new FormControl("", Validators.required),
      image_hold_pid: new FormControl("", Validators.required),
    });
  }

  ngOnInit() {
  }
  readURL(input) {
    return new Promise((reslove,reject)=>{
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e:any)=> {
          reslove(e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }else{
        reslove(undefined);
      }
    })

  }
  file(e,control) {
    this.zone.run(()=>{
      this.readURL(e.target).then(e=>{
        this.previewBase64[control] = e;
      })
      this.step2Form.patchValue({[control]: e.target.files[0]})
    })
  }
  get getFormControl() {
    return this.step2Form.controls;
  }
  back(){
    this.navCtrl.navigateBack("/kyc/step1");
  }
  onSubmit(){
    this.zone.run(()=>{
      this.submitted = true;
      if (!this.step2Form.invalid) {
        this.loading = true;
        this.kyc.SaveState(this.step2Form.value,2)
        .then(()=>{
          this.loading = false;
          this.navCtrl.navigateBack("/kyc/step3");
        }).catch(err=>{
          this.loading = false;
        })
        
      }else{
        console.log(this.step2Form.controls);
      }
    })
  }
}
