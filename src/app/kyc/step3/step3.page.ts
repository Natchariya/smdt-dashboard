import { Component, OnInit } from '@angular/core';
import { KycService } from 'src/app/services/kyc.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.page.html',
  styleUrls: ['./step3.page.scss', '../kyc.page.scss'],
})
export class Step3Page implements OnInit {
  state;
  constructor(public kyc: KycService,public navCtrl: NavController) {
    this.state = kyc.state;
  }

  ngOnInit() {
  }
  openBank(){
    this.navCtrl.navigateRoot("/bank-account");
  }
  new(){
    this.kyc.newSubmitRequest();
  }
}
