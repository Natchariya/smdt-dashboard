import { Component, OnInit, NgZone } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ValidateService } from 'src/app/services/validate.service';
import { TrainingService } from 'src/app/services/training.service';
import { AngularFireFunctions } from '@angular/fire/functions';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['../no-auth.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  err_message = "";
  loading = false;
  constructor(
    public navCtrl: NavController,
    private auth: AuthService,
    public validate: ValidateService,
    public zone:NgZone,
    public fns: AngularFireFunctions
    ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      tel: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required),
    });
    this.loginForm.valueChanges.subscribe(() => {
      this.err_message = "";
    })
  }
  toggleHelpMenu() {
    this.zone.run(()=>{
      this.navCtrl.navigateRoot("/usr/register")
    })
    
  }
  onFormSubmit(){
    this.zone.run(()=>{
      if (this.loginForm.valid) {
        let phone = this.validate.phone(this.loginForm.value.tel);
        if(phone){
          this.loading = true;
          this.auth.login(this.loginForm.value.tel,this.loginForm.value.password).then(res=>{
            this.loading = false;
            this.navCtrl.navigateRoot("/dashboard")
          },err=>{
            this.loading = false;
            console.log(err);
            
            if(err.error.code == 400){
              this.err_message = "ไม่พบข้อมูลผู้ใช้"
            }else if(err.error.code == 401){
              this.err_message = "รหัสผ่านไม่ถูกต้อง"
            }
            
          })
        }else{
          this.err_message = "เบอร์โทรศัทพ์ไม่ถูกต้อง";
        }
        
      }else{
        this.err_message = "กรุณากรอกข้อมูลให้ครบถ้วน";
      }
      // this.auth.login(this.loginForm.value.tel,this.loginForm.value.password)
    })

  }
}
