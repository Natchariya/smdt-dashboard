import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { environment } from "../../environments/environment";
@Component({
  selector: 'app-no-auth',
  templateUrl: './no-auth.page.html',
  styleUrls: ['./no-auth.page.scss'],
})
export class NoAuthPage implements OnInit {
  @ViewChild('expandable', { static: false }) expandable;
  sideImage;
  server = environment.server_version;
  constructor(public menuCtrl: MenuController,public router: Router,public navCtrl: NavController) { }

  ngOnInit() {
    this.router.events.subscribe(ob=>{
      const path = window.location.pathname.split('/')[2];
      if(path == "login"){
        this.sideImage = true;
      }else{
        this.sideImage = false;
      }
      // this.updateHeight();
    })

  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
  updateHeight() {
    const el = this.expandable.nativeElement;

    setTimeout(() => {

      const prevHeight = el.style.height;
      el.style.height = 'auto';
      const newHeight = el.scrollHeight + 'px';
      el.style.height = prevHeight;

      setTimeout(() => {
        el.style.height = newHeight;
      }, 0);
    }, 0);
  }
}
