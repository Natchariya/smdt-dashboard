import { Component, OnInit, NgZone } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['../no-auth.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  err_message = "";
  loading = false;
  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    private auth: AuthService,
    public zone: NgZone) {

  }

  ngOnInit() {
    this.registerForm = new FormGroup({
      uid: new FormControl("", Validators.required),
      name: new FormControl("", Validators.required),
      tel: new FormControl("", Validators.required),
      password: new FormControl("", [Validators.required, Validators.minLength(6)]),
      confirm_password: new FormControl("", Validators.required),
    },
      {
        validators: this.MatchPassword('password', 'confirm_password')
      }
    )
    this.registerForm.valueChanges.subscribe(() => {
      this.err_message = "";
    })
  }
  toggleHelpMenu() {
    this.zone.run(() => {
      this.navCtrl.navigateRoot("/usr/login")
    })

  }
  onFormSubmit() {
    this.zone.run(() => {
      if (this.registerForm.valid) {
        this.loading = true;
        this.auth.register(this.registerForm.value).subscribe(
          res => {
            this.loading = false;

            this.navCtrl.navigateRoot("/usr/login")

            // this.navCtrl.navigateRoot("/usr/action/login")
          }
          , err => {
            setTimeout(() => {
              this.loading = false;
            }, 1000);

            this.err_message = err.error.message
          })
      } else {
        if (this.registerForm.invalid) {
          this.err_message = "กรุณากรอกข้อมูลให้ครบถ้วน"
        }
        if (this.registerForm.controls['confirm_password'].errors.passwordMismatch) {
          this.err_message = "กรุณากรอกพาสเวิร์ดให้ตรงกัน"
        }

      }
    })
  }
  ValidatehUid(uid: string) {
    return (formGroup: FormGroup) => {
      const UID = formGroup.controls[uid];
      if (!UID) {
        return null;
      }
      if (UID.value.length == 9) {
        UID.setErrors({ UID_invalid: true });
      } else {
        UID.setErrors(null);
      }
    }
  }
  MatchPassword(password: string, confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const passwordControl = formGroup.controls[password];
      const confirmPasswordControl = formGroup.controls[confirmPassword];

      if (!passwordControl || !confirmPasswordControl) {
        return null;
      }

      if (confirmPasswordControl.errors && !confirmPasswordControl.errors.passwordMismatch) {
        return null;
      }

      if (passwordControl.value !== confirmPasswordControl.value) {
        confirmPasswordControl.setErrors({ passwordMismatch: true });
      } else {
        confirmPasswordControl.setErrors(null);
      }
    }
  }

}
