import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient } from '@angular/common/http';
import { AngularFireFunctions } from '@angular/fire/functions';
import { environment } from "../../environments/environment";
import * as firebase from 'firebase/app';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private root = environment.function_url;
  // private root = "http://localhost:5001/smdt-website-preprod/us-central1";
  constructor(private auth: AngularFireAuth, private http: HttpClient, private fnc: AngularFireFunctions) {
    this.auth.currentUser.then(res => {

    })
  }
  createToken() {


  }
  public register(registerdata) {
    let data = registerdata;
    let name = data.name.split(" ")

    return this.http.post(`${this.root}/register`, JSON.stringify({
      uid: data.uid,
      firstname: name[0],
      lastname: name[1],
      phone: data.tel,
      password: data.password,
    }))
  }
  public login(tel, password) {
    return new Promise((reslove, reject) => {
      this.http.post(`${this.root}/token`, JSON.stringify({
        tel: tel,
        password: password,
      })).subscribe((res:any)=>{
        this.auth.setPersistence(firebase.auth.Auth.Persistence.SESSION).then(()=>{
          this.auth.signInWithCustomToken(res.token).then(res=>{
            reslove();
          }).catch(err=>{
            reject(err) // Signin Error
          })
        },err=>{
          reject(err) // setPersistence Error
        })

      },err=>{
        reject(err) // client Error
        
      })
    })
  }
  public logout() {
    return this.auth.signOut().then(() => {
      window.location.reload();
    })
  }
}
