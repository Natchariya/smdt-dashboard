import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import { AngularFireStorage } from '@angular/fire/storage';

interface bank {
  bank_name: String;
  account_number: String;
  account_type: String;
  account_holder_firstname: String;
  account_holder_lastname: String;
  account_branch: String;
  image_book_and_id: any;
}
@Injectable({
  providedIn: 'root'
})
export class BankService {
  private uid = firebase.auth().currentUser.uid;
  private _loading = new BehaviorSubject(false);
  private _bank_account = new BehaviorSubject([]);
  private query = {
    path: `users/${this.uid}/bank_account`,
    limit: 2,
    prepend: false
  };;

  data: Observable<any[]>;
  loading: Observable<boolean> = this._loading.asObservable();


  constructor(private afs: AngularFirestore, private stroage: AngularFireStorage) {
    this.loading = this._loading.asObservable();
  }
  request_bank(request: bank) {
    console.log("dsf");
    
    return new Promise((reslove, reject) => {
      if (request.image_book_and_id) {
        let file = request.image_book_and_id;
        let type = file.type.split("/");
        let ser = type[type.length-1];
        let name = `user_bank_account/${this.uid}_${new Date().getTime()}.${ser}`;
        // Create file metadata including the content type
        var metadata = {
          contentType: file.type
        };

        this.stroage.upload(`${name}`, request.image_book_and_id,metadata).then(res=>{
          request.image_book_and_id = name;
          request['createAt'] = firebase.firestore.FieldValue.serverTimestamp()
          this.genQuery().add(request).then(res=>{
            reslove(res)
          }).catch(err=>{
            reject(err);
          })
        }).catch(err=>{
          reject(err);
        })
      }else{
        reject();
      }
    })
  }
  genQuery(): any {
    const q = this.query.path.split("/");
    let queryFn: any = this.afs;
    q.forEach((element, index) => {
      if (index == 0) {
        queryFn = queryFn.collection(element, q => q.limit(this.query.limit));
      } else {
        if (index % 2 == 0) {
          queryFn = queryFn.collection(element);
        } else {
          queryFn = queryFn.doc(element)
        }
      }

    });
    return queryFn;
  }
  get_bank() {
    this.data = this._bank_account.asObservable();
    return this.genQuery().valueChanges().subscribe((res: []) => {
      this._bank_account.next(res);
    })
  }
}
