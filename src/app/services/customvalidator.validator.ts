import { ValidatorFn, AbstractControl, FormControl } from '@angular/forms';

export const setError = (control: any): { [key: string]: string } => {
    // console.log(control);
    let controls = control.controls;
    for (let [key, value] of Object.entries(controls)) {
        if(controls[key].errors){
            let error = controls[key].errors;
            let con = controls[key];
            for (let [key, value] of Object.entries(error)) {
                // console.log(`${key}: ${value}`);
                if(con.errors){
                    if (value) {
                        switch (key) {
                            case "required":
                                con.errors['message'] = "กรุณากรอกข้อมูล";
                                break;
                            case "pattern":
                                con.errors['message'] = "ข้อมูลไม่ถูกต้อง";
                                break;
                            default:
                                break;
                        }
                    }
                }
    
        
            }
        }
    }
    return null;
};
export function MoreThenTwentry(control: FormControl) {
    let now:Date = new Date();
    let maxDate = new Date(now.getFullYear() - 20,now.getMonth(),now.getDate()).getTime();
    if(!control.value){
        return null
    }
    let select_time = toTime(control.value);
    if(select_time < maxDate){
        return null
    }else{
        return { minAge : true, message: "คุณอายุไม่ถึง 20 ปี"}
    }
    
};
function toTime(params:string) {
    let date_input = params.split("-");
    return new Date(params).getTime();
}