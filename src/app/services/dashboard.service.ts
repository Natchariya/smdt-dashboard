import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { BehaviorSubject, Observable } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private uid = firebase.auth().currentUser.uid;
  private _loading = new BehaviorSubject(false);
  private _contract_data = new BehaviorSubject([]);
  private _commit = new BehaviorSubject({});
  private _sum_income = new BehaviorSubject(0);
  private _graph_data= new BehaviorSubject({});
  private query = {
    path: `${this.uid}`,
    prepend: false
  };;
  commit_data: Observable<any>;
  data: Observable<any[]>;
  sum_income: Observable<any>;
  graph_data: Observable<any>;

  contracts_sum = 0;
  commit_sum = 0;
  now = new Date().getTime();

  interval;

  loading: Observable<boolean> = this._loading.asObservable();
  constructor(private rdb: AngularFireDatabase) {
    this.sum_income = this._sum_income.asObservable();
    this.graph_data = this._graph_data.asObservable();
    this._sum_income.next(0);
    
  }
  public getCommition() {
    let now = new Date().getTime();
    this.commit_data = this._commit.asObservable();
    return this.rdb.object("commit_users/" + this.query.path).valueChanges().subscribe((res: any) => {
      if(res){
        this.commit_sum = res.commit_price;
        this.upDateSumIncome();
        this._commit.next(res);
      }else{
        this.commit_sum = 0;
        this.upDateSumIncome();
        this._commit.next(res);
      }
      
    },e=>{
      
    })
  }
  upDateSumIncome() {
    this._sum_income.next(this.contracts_sum + this.commit_sum);
  }
  upDateLocalDash(res) {
    this.now = new Date().getTime();
    let ob: any = { contract: res };
    let sum_income_contract = 0;
    ob.contract.forEach((element: any) => {
      element.days_left = (this.toDays(element.end - this.now));
      if(element.end < this.now){
        element.contract_income = (this.toSec(element.end - element.start) * (7.2/86400)) * element.blocks;
      }else{
        element.contract_income = (this.toSec(this.now - element.start) * (7.2/86400)) * element.blocks;
      }
      sum_income_contract += element.contract_income;
    });
    ob.all_contract_income = sum_income_contract;
    this.contracts_sum = ob.all_contract_income;
    this.upDateSumIncome();
   
    this._contract_data.next(ob);
  }

  InterValUpdate(res){
    this.upDateLocalDash(res);
    
    clearInterval(this.interval);
    this.interval = setInterval(() => {
      this.upDateLocalDash(res);
    }, 5000);
  }
  getContacts() {
    this.data = this._contract_data.asObservable();
    return this.rdb.list("contracts_users/" + this.query.path).valueChanges().subscribe((res: []) => {
      this.InterValUpdate(res);
      this.getGraph(this.getDays(),res);
    },e=>{

    })
  }
  toSec(ms) {
    return ms / 1000;
  }
  millisToMinutesAndSeconds(millis) : number {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000);
    return seconds;
  }
  toDays(ms) {
    if (ms > 0) {
      let sec = (ms / 1000);
      let min = (sec / 60);
      let hours = (min / 60);
      let days = (hours / 24);
      return Math.floor(days);
    }
    return 0;
  }
  getGraph(days,contracts){
    
    let graph = {
      days: [],
      data: []
    };
    days.forEach(d => {
      // console.log(element);
      let y = new Date().getFullYear();
      let m = new Date().getMonth();
      let date = new Date(y,m,d).getTime();
      let income = 0;
      let now = new Date().getTime();
      contracts.forEach(block => {
        // console.table(new Date(block.start).toLocaleString(),new Date(date).toLocaleString(),new Date(block.end).toLocaleString());
        if(date < now){
          if((block.start < date) && (block.end > date)){
            income += (7.2 * block.blocks);
          // if(block.end < date){
          //   income = (this.toSec(block.end - block.start) * (7.2/86400)) * block.blocks;
          // }else{
          //   income = (this.toSec(date - block.start) * (7.2/86400)) * block.blocks;
          // }
        }
        }

      });
      graph.days.push(d);
      graph.data.push(income);
    });
    this._graph_data.next(graph);
    // console.log(graph);
    
  }
  getDays(){
    let i = new Date(new Date().getFullYear(), 5, 0).getDate();
    let days = [];
    for (let index = 1; index < i+1; index++) {
      days.push(index);
    }
    return days;
  }
}
