import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { AngularFireStorage } from '@angular/fire/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class KycService {
  kycForm;
  isStart = false;
  test = true;
  private _state = new BehaviorSubject({});
  state: Observable<any>;
  private uid = firebase.auth().currentUser.uid;
  constructor(private fdb: AngularFirestore, private stroage: AngularFireStorage) {

    this.state = this._state.asObservable();
    this.getForm();
  }
  getState(){
    this.fdb.collection("users_kyc").doc(this.uid).valueChanges().subscribe((res:any)=>{
      if(res){
        if(res.kyc_state == "reject"){
          this._state.next({state : res.kyc_state,reason: res.kyc_reject_reason})
        }else{
          this._state.next({state : res.kyc_state})
        }
      }else{
        this._state.next({state : ""})
      }

      
    })
  }
  get isActivate() {
    return this.test;
  }
  newSubmitRequest(){
    this.fdb.collection("users_kyc").doc(this.uid).update({kyc_state: ""})
  }
  getForm() {
    let form = localStorage.getItem("kyc_siammodnitech.com");
    if (form) {
      return this.kycForm = JSON.parse(form);
    }
  }
  SaveState(formValue, step) {
    switch (step) {
      case 1:
        this.kycForm = formValue;
        let formString = JSON.stringify(formValue);
        localStorage.setItem(`kyc_siammodnitech.com`, formString);
        break;
      case 2:
        this.kycForm = { ...this.kycForm, ...formValue };
        return this.submit_set(this.kycForm);
        break;
      default:
        break;
    }

  }
  upload(name,request,metadata){
    return new Promise((resolve,reject)=>{
      this.stroage.upload(`${name}`, request, metadata).then(res=>{
        resolve(res);
      }).catch(err=>{
        reject(err)
      })
    })
  }
  submit_set(request) {
    return new Promise((reslove, reject) => {
      let images_name = ["image_front_pid", "image_back_pid", "image_hold_pid"]
      if (request.image_front_pid && request.image_back_pid && request.image_hold_pid) {
        let files = [request.image_front_pid, request.image_back_pid, request.image_hold_pid];
        Promise.all(
          // Array of "Promises"
          files.map((file, index) => {
            let type = file.type.split("/");
            let ser = type[type.length - 1];
            let name = `kyc_users/${this.uid}/${images_name[index]}_${new Date().getTime()}.${ser}`;
            // Create file metadata including the content type
            var metadata = {
              contentType: file.type
            };
            this.upload(name,file,metadata);
            return name;
          })
          
        )
          .then((url) => {
            images_name.forEach((element,index) => {
              request[element] = url[index];
            });
            request['createAt'] = firebase.firestore.FieldValue.serverTimestamp();
            request['kyc_state'] = "reviewing";
            this.fdb.collection("users_kyc").doc(this.uid).set(request).then(res => {
              this.distory();
              reslove({message: "success"});
            }).catch(err => {
              reject(err);
            })
          })
          .catch((error) => {
            reject(error);
          });
      } else {
        reject();
      }
    })
  }
  distory() {
    localStorage.removeItem(`kyc_siammodnitech.com`);
  }
}
