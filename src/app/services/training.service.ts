import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireFunctions } from '@angular/fire/functions';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {
  private uid = firebase.auth().currentUser.uid;
  private _state = new BehaviorSubject(undefined);
  private _loading = new BehaviorSubject(false);

  state: Observable<any>;
  loading: Observable<boolean> = this._loading.asObservable();
  constructor(public afDb: AngularFirestore,private stroage: AngularFireStorage,private fns: AngularFireFunctions) { 

  }
  init(){
    this._loading.next(true);
    this.state = this._state.asObservable();
    return this.afDb.collection("users_training").doc(this.uid).valueChanges().subscribe(res=>{
      this._state.next(res);
      this._loading.next(false);
    },e=>{
      this._loading.next(false);
    })
  }
  startWatchVideo(){
    return this.afDb.collection("users_training").doc(this.uid).update({
      start_watch: firebase.firestore.FieldValue.serverTimestamp()
    })
  }
  VideoWatched(){
    return this.afDb.collection("users_training").doc(this.uid).set({
      watched: firebase.firestore.FieldValue.serverTimestamp()
    },{merge: true})
  }
  startExam(){
    return this.afDb.collection("users_training").doc(this.uid).update({
      startExam: firebase.firestore.FieldValue.serverTimestamp()
    })
  }
  getVideo(){
    return new Promise((resolve,reject)=>{
       this.afDb.collection("training").doc("course").get().subscribe(res=>{
        this.stroage.ref(res.data().media).getDownloadURL().subscribe(url=>{
            resolve(url);
        })
      })
    })
  }

  getExam(){
    return this.afDb.collection("training").doc("course").get()
  }
  checkExam(question){
    console.log("cal");
    const callable = this.fns.httpsCallable('examChecker');
    let data$ = callable(question);
    return data$;
  }
}
