import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public userData:firebase.User;
  public Uid:string;
  constructor(private auth: AngularFireAuth) { 
    this.auth.authState.subscribe(user=>{
      if(user){
        this.userData = user;
        this.Uid = user.uid;
      }else{
        this.userData = undefined;
      }
    })

  }
  setUserData(data){
    
  }
  get userUid(){
    return this.Uid;
  }
}
