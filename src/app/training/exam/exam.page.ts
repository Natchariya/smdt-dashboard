import { Component, OnInit, NgZone } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { TrainingService } from 'src/app/services/training.service';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.page.html',
  styleUrls: ['./exam.page.scss'],
})
export class ExamPage implements OnInit {
  question = [];
  active_clause = 0;
  exam_end = false;
  answer = "";
  process_exam = 0;
  show_point = false;
  result;
  err;
  state;
  inputerr = false;
  constructor(public menuCtrl: MenuController, public zone: NgZone, public navCtrl: NavController, public training: TrainingService) { }

  ngOnInit() {
    this.state = this.training.state;
    this.training.getExam().subscribe(res => {
      let q = res.data().exam;
      q.forEach(element => {
        this.question.push({
          a: "",
          q: element,
          r: false
        })
      });
    })
    // this.process();
  }
  close() {
    this.navCtrl.navigateRoot("/training");
  }
  next_clause() {
    this.zone.run(() => {
      let an = this.answer.trim();
      if (an.length > 0) {
        this.question[this.active_clause]['a'] = this.answer;
        this.answer = "";
        this.active_clause++;
        this.inputerr = false;
        if (this.active_clause == this.question.length) {
          this.exam_end = true;
          this.process();
        }
      }else{
        this.inputerr = true;
      }
    })
  }


  process() {
    this.process_exam = 0;
    let inter = setInterval(() => {
      this.process_exam += Math.random() / 10;
      if (this.process_exam > 0.8) {
        clearInterval(inter);
      }
    }, 200);
    this.training.checkExam(this.question).subscribe(res => {
      this.process_exam = 1;
      this.show_point = true;
      this.result = res;

    }, err => {
      this.process_exam = 1;
      this.show_point = true;
      this.err = err;
    })
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
}
