import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { MenuController, ModalController, NavController } from '@ionic/angular';
import { TrainingService } from '../services/training.service';


@Component({
  selector: 'app-training',
  templateUrl: './training.page.html',
  styleUrls: ['./training.page.scss'],
})
export class TrainingPage implements OnInit {
  @ViewChild('player', { static: false }) player: any;
  isPlaying = false;
  state;
  loading: any;
  videoPath;
  progressBar = 0;
  isCopy = false;
  new = new Date().getTime();
  ban;
  constructor(public menuCtrl: MenuController, public zone: NgZone, public navCtrl: NavController, public training: TrainingService) { }

  ngOnInit() {
    this.loading = this.training.loading;
    this.training.init();
    this.state = this.training.state;
    this.training.state.subscribe(res=>{
      if(res){
        this.ban = res.isBan.toMillis();
      }
     
    })
    this.training.getVideo().then(res => {
      // console.log(res);
      this.player.nativeElement.setAttribute('src', res);
      this.videoPath = res;
    })
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
  ngAfterViewInit() {
    this.player.nativeElement.addEventListener("ended", async end => {
      this.isPlaying = false;
      await this.training.VideoWatched()
    });
    this.player.nativeElement.addEventListener("timeupdate", async d => {
      this.updateProgressBar(this.player.nativeElement);
    });

  }
  updateProgressBar(mediaPlayer) {
    var percentage = (Math.floor((100 / mediaPlayer.duration) * mediaPlayer.currentTime));
    this.progressBar = percentage / 100;

  }
  exam() {
    this.navCtrl.navigateForward("training/exam")
  }
  playpause() {
    this.zone.run(() => {
      if (!this.isPlaying) {
        this.player.nativeElement.play();
        this.isPlaying = true;
      } else {
        this.player.nativeElement.pause();
        this.isPlaying = false;
      }
    })

  }
  copycode() {
    this.training.state.subscribe(res => {
      console.log(res);
      
      this.copyMessage(res.reference);
      this.isCopy = true;
      setTimeout(() => {
        this.isCopy = false;
      }, 550);
      
    })
  }
  copyMessage(val: string){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
  lineOpen(){
    window.open("https://line.me/ti/g2/jluGStI9qXoEx33Boy4AqA","_blank")
  }
}
