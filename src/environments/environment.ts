// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  server_version: "1.0.8_Staging_server @ 2020",
  function_url: "https://asia-east2-smdt-website-preprod.cloudfunctions.net",
  firebase:{
    apiKey: "AIzaSyA1QiuNWpFk-RqPQa1dH6w7arm1so5jydM",
    authDomain: "smdt-website-preprod.firebaseapp.com",
    databaseURL: "https://smdt-website-preprod.firebaseio.com",
    projectId: "smdt-website-preprod",
    storageBucket: "smdt-website-preprod.appspot.com",
    messagingSenderId: "873783921266",
    appId: "1:873783921266:web:7050446808a799f01650eb"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
